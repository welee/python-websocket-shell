#-*- coding:utf-8 -*-
import pysvn
import os
import sys
import time
import json
workpath='/work/ModemWarface/dataassets/Assets/';
#由于文件的加密密钥变更需要重新加载所有的资源否则解密会失败，所以增加一个全局的额外版本号
addversion=1;
client = pysvn.Client();
client.update(workpath);
jlist={} 
def res_list( ):
    all_files = client.list('http://192.168.23.12:81/svn/ModemWarface/dataassets/Assets/res/',revision=pysvn.Revision( pysvn.opt_revision_kind.head ), recurse=True )
    for file, Q in all_files:
        args = {}
        args.update( file )
        path=args['path'];
        path=path.replace('http://192.168.23.12:81/svn/ModemWarface/dataassets/Assets/','')
        index=path.find('.')
        if index==-1:
            continue
        if path.endswith('meta'):
            continue
        path=path[0:index]
        jlist[path]=[file.created_rev.number+addversion,0]
 


def config_list( ):
    all_files = client.list('http://192.168.23.12:81/svn/ModemWarface/client/dataconfig',revision=pysvn.Revision( pysvn.opt_revision_kind.head ), recurse=True )
    num=0;
    for file, Q in all_files:
        args = {}
        args.update( file )
        path=args['path'];
        if path.find('.')==-1:
            continue
        if path.endswith('meta'):
            continue
        num+=file.created_rev.number+addversion;
    jlist['dataconfig/dataconfig']=[num,1]

f=open('version.php','w');
config_list();
#res_list();
print >>f, ('<?php \n $jsonstr=\'%s\' ;\n $jsonstr=json_decode($jsonstr);\n  echo json_encode($jsonstr);\n ?>' % json.dumps(jlist,False,False))
f.close()
