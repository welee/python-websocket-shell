#!/bin/bash
cd /work/ModemWarface/client/dataconfig
svn up
cd /work/ModemWarface/server
svn up
cd /work/ModemWarface/dataassets/Assets
svn up
cd /work/ModemWarface/zsxdres/
rm Assets/XingYu/data/dataconfig.bytes
rm Assets/BundleManager/BuildStates.txt
svn up
cd /work/ModemWarface/zsxdres-win/
rm Assets/XingYu/data/dataconfig.bytes
rm Assets/BundleManager/BuildStates.txt
svn up
rm /work/ModemWarface/dataassets/Assets/Plugins/bin/Assembly-CSharp-firstpass.dll*
rm -r /work/ModemWarface/dataassets/Assets/dataconfig/*
cp -r /work/ModemWarface/client/dataconfig/* /work/ModemWarface/dataassets/Assets/dataconfig/

cd /work/ModemWarface/android/dataassets/
svn up
cd /work/ModemWarface/android/zsxdres/
rm Assets/XingYu/data/dataconfig.bytes
rm Assets/BundleManager/BuildStates.txt
svn up

rm /work/ModemWarface/android/dataassets/Assets/Plugins/bin/Assembly-CSharp-firstpass.dll*
rm -r /work/ModemWarface/android/dataassets/Assets/dataconfig/*
cp -r /work/ModemWarface/client/dataconfig/*  /work/ModemWarface/android/dataassets/Assets/dataconfig/
