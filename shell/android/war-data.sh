#!/usr/bin/expect
set work "/work/ModemWarface/android/dataassets"
set keypath "/work/ModemWarface/shell/.mykey/"
log_file "$work/../shell/export-data.log"
set timeout 3600
spawn "/Applications/Unity/Unity.app/Contents/MacOS/Unity" -quit -batchmode -projectPath "$work" -executeMethod ExportAssetBundles.ExportResourceTrack -logFile "./export-data.log"
expect eof
cd $work/Assets/temp/
spawn bash -c "python /work/ModemWarface/shell/python-tool/version.py"
expect eof
spawn bash -c "find $work/Assets/temp/ -name '*.meta'  -type f -print | xargs /bin/rm -f"
expect eof
spawn bash -c "scp -P8844 -r -i $keypath/rongjun $work/Assets/temp/* root@122.5.45.178:/opt/tankwar/android_config/"
expect eof
puts "end!!!!"
