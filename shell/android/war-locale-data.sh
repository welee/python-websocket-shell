#!/usr/bin/expect
set work "/work/ModemWarface/android/dataassets/"
set keypath "/work/ModemWarface/shell/.mykey/"
log_file "$work/../shell/export-data.log"
set timeout 3600

spawn "/work/ModemWarface/android/shell/svn-up.sh"
expect eof
spawn "/Applications/Unity/Unity.app/Contents/MacOS/Unity" -quit -batchmode -projectPath "$work" -executeMethod ExportAssetBundles.ExportResourceTrack -logFile "./export-data.log"
expect eof
cd $work/Assets/temp/
spawn bash -c "python /work/ModemWarface/shell/python-tool/version.py"
expect eof
spawn bash -c "find $work/Assets/temp/ -name '*.meta'  -type f -print | xargs /bin/rm -f"
expect eof
spawn bash -c "scp -r -i $keypath/id_dsa_1024_31  $work/Assets/temp/* root@192.168.23.31:/var/www/tankwar/android/res/"
expect {Enter passphrase for key}
send "********\n"
expect eof
puts "end!!!!"
