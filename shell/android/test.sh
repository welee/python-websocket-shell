#!/usr/bin/expect
set work "/work/ModemWarface/android/client"
set zdate [exec date +%Y%m%d-%H%M%S]

#写日志的函数
proc log {msg} {
#写日志的同时将消息打印在屏幕上
        puts "$msg\n"
        send_log "$msg\n"
}

log_file "$work/../shell/android/export-ipa.log"
set timeout 3600
spawn "/Applications/Unity/Unity.app/Contents/MacOS/Unity" -quit -batchmode -projectPath "$work" -executeMethod ExportAssetBundles.BuildIOS -logFile "$work/../shell/android/export-app.log"
expect eof

puts "end!!!!"
